package vu.co.sudhirxps.lablock.pojo;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Lock {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("state")
    @Expose
    private Boolean state;
    @SerializedName("users")
    @Expose
    private List<String> users = new ArrayList<String>();

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The state
     */
    public Boolean getState() {
        return state;
    }

    /**
     *
     * @param state
     * The state
     */
    public void setState(Boolean state) {
        this.state = state;
    }

    /**
     *
     * @return
     * The users
     */
    public List<String> getUsers() {
        return users;
    }

    /**
     *
     * @param users
     * The users
     */
    public void setUsers(List<String> users) {
        this.users = users;
    }

}