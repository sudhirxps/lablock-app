package vu.co.sudhirxps.lablock;

import java.util.List;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;
import vu.co.sudhirxps.lablock.pojo.Lock;
import vu.co.sudhirxps.lablock.pojo.Token;
import vu.co.sudhirxps.lablock.pojo.User;

/**
 * Created by sudhir on 17/11/15.
 */
public interface RESTServerApi {


    String API_URL = "http://10.0.2.2:8000";

    @GET("/lock/")
    Call<List<Lock>> listLocks(@Query("users") String user);

    @PUT("/lock/{id}/")
    Call<Lock> lock(@Body Lock lock,@Path("id" ) String id);

    @POST("/users/")
    Call<User> addUser(@Body User user);

    @GET("/users/")
    Call<User> getUser(@Query("email") String email );

    @POST("/api-token-auth/")
    Call<Token> getAuthToken(@Body User user);
}

