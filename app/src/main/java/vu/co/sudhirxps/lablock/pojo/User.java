package vu.co.sudhirxps.lablock.pojo;

/**
 * Created by admin7 on 20/11/15.
 */
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("first_name")
    @Expose
    private String first_name;

    @SerializedName("email")
    @Expose
    private String email;

    public String getPasssword() {
        return passsword;
    }

    public void setPasssword(String passsword) {
        this.passsword = passsword;
    }

    @SerializedName("password")
    @Expose
    private String passsword;


    @SerializedName("groups")
    @Expose
    private List<Object> groups = new ArrayList<Object>();



    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     *
     * @return
     * The username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     * The username
     */
    public void setUsername(String username) {
        this.username = username;
    }
    /**
     *
     * @return
     * The first_name
     */
    public String getFirstName() {
        return first_name;
    }

    /**
     *
     * @param first_name
     * The email
     */
    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }
    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The groups
     */
    public List<Object> getGroups() {
        return groups;
    }

    /**
     *
     * @param groups
     * The groups
     */
    public void setGroups(List<Object> groups) {
        this.groups = groups;
    }

}

