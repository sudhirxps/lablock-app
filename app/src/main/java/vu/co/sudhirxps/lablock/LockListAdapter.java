package vu.co.sudhirxps.lablock;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import java.io.IOException;
import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import vu.co.sudhirxps.lablock.pojo.Lock;

public class LockListAdapter extends ArrayAdapter<Lock> {


    private Context context;

    private ArrayList<Lock> data;

    public LockListAdapter(Context context, ArrayList<Lock> locks) {
        super(context, 0, locks);
        this.context = context;
        this.data = locks;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Lock lock = (Lock) getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_item_switch, parent, false);
        }
        // Lookup view for data population
        Switch switchButton = (Switch) convertView.findViewById(R.id.switch1);
        TextView labName = (TextView) convertView.findViewById(R.id.labName);
        // Populate the data into the template view using the data object

        switchButton.setChecked(lock.getState());
        labName.setText(lock.getName());



        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                Lock lock = data.get(position);
                lock.setState(isChecked);
                changeLockState(lock);
            }
        });

        // Return the completed view to render on screen
        return convertView;
    }

    private void changeLockState(Lock lock){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        String username = preferences.getString("url",null);
        final String password = preferences.getString("token", null);

        // Define the interceptor, add authentication headers
        Interceptor interceptor = new Interceptor() {
            @Override
            public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder().addHeader("Authorization", " Token "+password).build();
                return chain.proceed(newRequest);
            }
        };

        // Add the interceptor to OkHttpClient
        OkHttpClient client = new OkHttpClient();
        client.interceptors().add(interceptor);

        // Set the custom client when building adapter
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RESTServerApi.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        RESTServerApi service = retrofit.create(RESTServerApi.class);

        String lockId = lock.getUrl().replace(RESTServerApi.API_URL+"/lock/","").split("/")[0];

        Call<Lock> lockCall = service.lock(lock,lockId);
        lockCall.enqueue(new Callback<Lock>() {
            @Override
            public void onResponse(Response<Lock> response, Retrofit retrofit) {
                Toast.makeText(context, "Lock state changed.",
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

    }

}