package vu.co.sudhirxps.lablock;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import vu.co.sudhirxps.lablock.pojo.Lock;
import vu.co.sudhirxps.lablock.pojo.Token;
import vu.co.sudhirxps.lablock.pojo.User;

public class SignedInActivity extends AppCompatActivity {

    private static final String TAG = "LockList";
    private SwipeRefreshLayout swipeContainer;

    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signed_in);


        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                fetchLocks();
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        // Read user name and password from Shared Pref

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String username = preferences.getString("username", null);
        String password = preferences.getString("password", null);

        if(username != null && password != null){
            loginAndFetchLock();
        }else{
            // Close activity and revoke user access from google

        }

        //Check if SharedPrefrences exists or not
        // Store email in the shared preferences



    }

    private void loginAndFetchLock(){

        // Retrieve credentials
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String username = preferences.getString("username",null);
        String password = preferences.getString("password", null);

        if(username != null && password != null){
            // Set the custom client when building adapter
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RESTServerApi.API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RESTServerApi service = retrofit.create(RESTServerApi.class);

            User user = new User();
            user.setPasssword(password);
            user.setUsername(username);

            Call<Token> authCall = service.getAuthToken(user);
            authCall.enqueue(new Callback<Token>() {
                @Override
                public void onResponse(Response<Token> response, Retrofit retrofit) {

                    if(response.isSuccess()){
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("token", response.body().getToken());
                        editor.apply();

                        fetchLocks();
                    }


                }

                @Override
                public void onFailure(Throwable t) {
                    new Throwable(t);
                }
            });
        }
    }

    private void fetchLocks() {

        // Retrieve credentials
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String username = preferences.getString("url",null);
        final String password = preferences.getString("token", null);

        if(username != null && password != null){

            username = username.replace("http://10.0.2.2:8000/users/","").split("/")[0];
            // Define the interceptor, add authentication headers
            Interceptor interceptor = new Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                    Request newRequest = chain.request().newBuilder().addHeader("Authorization", " Token "+password).build();
                    return chain.proceed(newRequest);
                }
            };

            // Add the interceptor to OkHttpClient
            OkHttpClient client = new OkHttpClient();
            client.interceptors().add(interceptor);

            // Set the custom client when building adapter
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RESTServerApi.API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
            RESTServerApi service = retrofit.create(RESTServerApi.class);

            Call<List<Lock>> call =  service.listLocks(username);
            call.enqueue(new Callback<List<Lock>>() {
                @Override
                public void onResponse(Response<List<Lock>> response, Retrofit retrofit) {
                    ArrayList<Lock> arrayOfLocks = (ArrayList<Lock>) response.body();

                    LockListAdapter adapter = new LockListAdapter(getApplicationContext(), arrayOfLocks);


                    ((ListView)findViewById(R.id.listView)).setAdapter(adapter);

                    swipeContainer.setRefreshing(false);
                }

                @Override
                public void onFailure(Throwable t) {
                    new Throwable(t);
                }

            });
        }else{
            // Close activity and revoke user access from google

        }
        swipeContainer.setRefreshing(false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_signed_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(HomeActivity.mGoogleApiClient);
            if (opr.isDone()) {
                // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
                // and the GoogleSignInResult will be available instantly.
                Log.d(TAG, "Got cached sign-in");
                GoogleSignInResult result = opr.get();
                signOut(result);
            } else {
                // If the user has not previously signed in on this device or the sign-in has expired,
                // this asynchronous branch will attempt to sign in the user silently.  Cross-device
                // single sign-on will occur in this branch.
            /*showProgressDialog();*/
                opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                    @Override
                    public void onResult(GoogleSignInResult googleSignInResult) {
                    /*hideProgressDialog();*/
                        signOut(googleSignInResult);
                    }
                });
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void signOut(GoogleSignInResult result) {


        if(HomeActivity.mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(HomeActivity.mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            finish();
                        }
                    });
        }
    }
}
